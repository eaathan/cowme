﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Excel;

namespace CowME
{
    public partial class CowMePane : UserControl
    {
        #region Constructor and Attributes
        Excel.Worksheet sheetit;
        Excel.Worksheet activeWorksheet;
        Excel.Application ExApp;

        public CowMePane()
        {
            InitializeComponent();
            textBox1.Text = "Press the button below to execute calculations. Remember to select a range in the sheet";
        }
        #endregion

        #region GUI Events

        private void _mkeSheetBtn_Click(object sender, EventArgs e)
        {
            DoCalculations();
        }
        #endregion

        #region Calculations methods
        /// <summary>
        /// Initialiazes the variables to start the Excel calculations
        /// and returns the selected cells from the sheet
        /// </summary>
        /// <returns>The selected cells from the sheet</returns>
        private Excel.Range InitializeSelected()
        {
            ExApp = Globals.ThisAddIn.Application as Excel.Application;
            activeWorksheet = ((Excel.Worksheet)ExApp.ActiveSheet);
            Excel.Workbook nativeWorkbook =
                ExApp.ActiveWorkbook;
            Excel.Range selection = ExApp.Selection as Excel.Range;
            if (selection != null && selection.Columns.Count == 4 && selection.Rows.Count > 0)
            {
                if (nativeWorkbook != null)
                {
                    Microsoft.Office.Tools.Excel.Workbook vstoWorkbook =
                        Globals.Factory.GetVstoObject(nativeWorkbook);
                    sheetit = (Excel.Worksheet)vstoWorkbook.Worksheets.Add();
                    try
                    {
                        sheetit.Name = "Cow Data";
                    }
                    catch
                    {
                        sheetit.Delete();
                        throw new Exception("Sheet already exists!");
                    }
                    sheetit.Application.ActiveWindow.SplitRow = 1;
                    sheetit.Application.ActiveWindow.FreezePanes = true;
                    return selection;
                }
            }
            else
            {
                MessageBox.Show("Selection is invalid!");
            }
            return null;
        }

        /// <summary>
        /// Set the header columns in the new sheet
        /// </summary>
        /// <param name="rCnt">row number</param>
        /// <param name="SelectedRange">the selected range from the original sheet </param>
        /// <returns>a dictionary of headers with a value of their column position</returns>
        private Dictionary<String, int> SetHeaders(int rCnt, Excel.Range SelectedRange)
        {
            Dictionary<String, int> columns = new Dictionary<string, int>();
            int counter = 3;
            for (rCnt = 1; rCnt <= SelectedRange.Rows.Count; rCnt++)
            {
                String col = (SelectedRange.Cells[rCnt, 3] as Excel.Range).Value;
                if (!String.IsNullOrEmpty(col) && !columns.Keys.Contains(col))
                {
                    columns.Add(col, counter++);
                }
            }

            //Create Columns

            (sheetit.Cells[1, 1] as Excel.Range).Value = "Cownr";
            (sheetit.Cells[1, 2] as Excel.Range).Value = "Date";
            foreach (var item in columns)
            {
                (sheetit.Cells[1, item.Value] as Excel.Range).Value = item.Key;
            }
            return columns;
        }

        /// <summary>
        /// Performs a number of calculations to rearrange columns and rows in the new sheet
        /// </summary>
        private void DoCalculations()
        {
            try
            {
                Excel.Range SelectedRange = InitializeSelected();
                int rCnt = 0;
                String cownr = null;
                DateTime date = new DateTime();
                int currentrow = 1;

                if (SelectedRange != null)
                {
                    Dictionary<String, int> columns = SetHeaders(rCnt, SelectedRange);
                    Dictionary<DateTime, List<InsertItem>> sortit = new Dictionary<DateTime, List<InsertItem>>();

                    //Set data
                    currentrow++;
                    for (rCnt = 1; rCnt <= SelectedRange.Rows.Count; rCnt++)
                    {
                        String newrowcow = (String)(SelectedRange.Cells[rCnt, 1] as Excel.Range).Value;
                        DateTime conv = new DateTime();
                        if ((SelectedRange.Cells[rCnt, 2] as Excel.Range).Value != null)
                        {
                            //do date conversion
                            DateTime.TryParse((SelectedRange.Cells[rCnt, 2] as Excel.Range).Value.ToString(), out conv);
                            DateTime newdate = ConvertExcelDate(conv);

                            //Skipping rows & inserting when new cow
                            List<InsertItem> objects = new List<InsertItem>();

                            if (newrowcow != cownr)
                            {
                                InsertCow(sortit, ref currentrow, cownr, columns);
                                sortit = new Dictionary<DateTime, List<InsertItem>>();
                                cownr = newrowcow;
                                date = newdate;
                                String name = (SelectedRange.Cells[rCnt, 3] as Excel.Range).Value;
                                double value = (SelectedRange.Cells[rCnt, 4] as Excel.Range).Value;
                                InsertValueToDictionary(ref objects, name, value, date);
                                sortit.Add(date, objects);
                                if (SelectedRange.Rows.Count == rCnt)
                                {
                                    InsertCow(sortit, ref currentrow, cownr, columns);
                                }
                            }
                            else if (SelectedRange.Rows.Count == rCnt)
                            {
                                sortit.TryGetValue(newdate, out objects);
                                String name = (SelectedRange.Cells[rCnt, 3] as Excel.Range).Value;
                                double value = (SelectedRange.Cells[rCnt, 4] as Excel.Range).Value;
                                InsertValueToDictionary(ref objects, name, value, date);
                                sortit[newdate] = objects;
                                InsertCow(sortit, ref currentrow, cownr, columns);
                            }
                            else
                            {
                                if (newdate != date)
                                {
                                    if (sortit.Keys.Contains(newdate))
                                    {
                                        sortit.TryGetValue(newdate, out objects);
                                        String name = (SelectedRange.Cells[rCnt, 3] as Excel.Range).Value;
                                        double value = (SelectedRange.Cells[rCnt, 4] as Excel.Range).Value;
                                        InsertValueToDictionary(ref objects, name, value, date);
                                        sortit[newdate] = objects;
                                    }
                                    else
                                    {
                                        cownr = newrowcow;
                                        date = newdate;
                                        String name = (SelectedRange.Cells[rCnt, 3] as Excel.Range).Value;
                                        double value = (SelectedRange.Cells[rCnt, 4] as Excel.Range).Value;
                                        InsertValueToDictionary(ref objects, name, value, date);
                                        sortit.Add(date, objects);
                                    }
                                }
                                else
                                {
                                    sortit.TryGetValue(newdate, out objects);
                                    String name = (SelectedRange.Cells[rCnt, 3] as Excel.Range).Value;
                                    double value = (SelectedRange.Cells[rCnt, 4] as Excel.Range).Value;
                                    InsertValueToDictionary(ref objects, name, value, date);
                                    sortit[newdate] = objects;
                                }
                            }
                        }
                    }
                    sheetit.Columns.AutoFit();
                }
            }
            catch (Exception exce)
            {
                MessageBox.Show(exce.Message + "\ndoes a sheet exists named Cow Data? \nplease delete this and try again");
            }
        }

        #region Helper Methods
        /// <summary>
        /// Converts bad dates from excel to danish format
        /// </summary>
        /// <param name="conv">Date to convert</param>
        /// <returns>converted date</returns>
        public DateTime ConvertExcelDate(DateTime conv)
        {
            DateTime newdate = conv;
            try
            {
                newdate = new DateTime(conv.Day + 2000, conv.Month, conv.Year - 2000);

            }
            catch
            {
                newdate = new DateTime(conv.Day + 2000, conv.Month, conv.Year - 1900);

            }
            return newdate;
        }

        /// <summary>
        /// Method for inserting cows in new sheet
        /// </summary>
        /// <param name="sortit">sorted dictionary of cownrs and insertitems</param>
        /// <param name="currentrow">The current row referenced, so value follows</param>
        /// <param name="cownr">the current cownr</param>
        /// <param name="columns">the header columns</param>
        private void InsertCow(Dictionary<DateTime, List<InsertItem>> sortit, ref int currentrow, string cownr, Dictionary<String, int> columns)
        {
            foreach (var item in sortit.Keys)
            {
                (sheetit.Cells[currentrow, 1] as Excel.Range).Value = cownr;
                (sheetit.Cells[currentrow, 2] as Excel.Range).Value = item;
                List<InsertItem> items = sortit[item];
                foreach (var insertitem in items)
                {
                    int place = 0;
                    columns.TryGetValue(insertitem.Name, out place);
                    (sheetit.Cells[currentrow, place] as Excel.Range).Value = insertitem.Value;
                }
                currentrow++;
            }
        }

        /// <summary>
        /// Method for inserting cow values into sorted dictionary
        /// </summary>
        /// <param name="objects">insertitems</param>
        /// <param name="name">column name</param>
        /// <param name="value">the value of the coulmn</param>
        /// <param name="date">the specific cow date</param>
        private void InsertValueToDictionary(ref List<InsertItem> objects,
            String name, double value, DateTime date)
        {
            InsertItem insitem = new InsertItem()
            {
                Name = name,
                Value = value
            };
            objects.Add(insitem);
        }

        #endregion Helper Methods
        #endregion Calculations methods
    }
    class InsertItem
    {
        public Double Value { get; set; }
        public String Name { get; set; }
    }
}
