﻿namespace CowME
{
    partial class CowFeatures : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public CowFeatures()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._mytab = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this._clickBtn = this.Factory.CreateRibbonButton();
            this._mytab.SuspendLayout();
            this.group1.SuspendLayout();
            // 
            // _mytab
            // 
            this._mytab.Groups.Add(this.group1);
            this._mytab.Label = "Cow ME";
            this._mytab.Name = "_mytab";
            // 
            // group1
            // 
            this.group1.Items.Add(this._clickBtn);
            this.group1.Label = "Cow Features";
            this.group1.Name = "group1";
            // 
            // _clickBtn
            // 
            this._clickBtn.Image = global::CowME.Properties.Resources.cow;
            this._clickBtn.Label = "Show Menu";
            this._clickBtn.Name = "_clickBtn";
            this._clickBtn.ShowImage = true;
            this._clickBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this._clickBtn_Click);
            // 
            // CowFeatures
            // 
            this.Name = "CowFeatures";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this._mytab);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.CowFeatures_Load);
            this._mytab.ResumeLayout(false);
            this._mytab.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab _mytab;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton _clickBtn;
    }

    partial class ThisRibbonCollection
    {
        internal CowFeatures CowFeatures
        {
            get { return this.GetRibbon<CowFeatures>(); }
        }
    }
}
